package project.micslo.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.stream.Collectors;


public class TapeTest
{
    private Tape tape1, tape2, tape3;
    private String w1, w2, w3;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp()
    {
        this.w1 = "abc"; // 3
        this.w2 = "aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd"; // 40
        this.w3 = "aaaaaaaaaabbbbbbbbbbccccccccccddddddddddaaaaaaaaaabbbbbbbbbbccccccccccdddddddddd"; // 80
        this.tape1 = new Tape(this.w1.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), null);
        this.tape2 = new Tape(this.w2.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), null);
        this.tape3 = new Tape(this.w3.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), null);
    }

    @Test
    public void shortWordInitTest()
    {
        String actualTape = "#abc############################";
        Assert.assertEquals(32, this.tape1.tape.size());
        Assert.assertEquals(actualTape.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), this.tape1.tape);
    }

    @Test
    public void longWordInitTest()
    {
        String actualTape = "#aaaaaaaaaabbbbbbbbbbccccccccccdddddddddd#######################";
        Assert.assertEquals(64, this.tape2.tape.size());
        Assert.assertEquals(actualTape.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), this.tape2.tape);
    }

    @Test
    public void longerWordInitTest()
    {
        String actualTape = "#aaaaaaaaaabbbbbbbbbbccccccccccddddddddddaaaaaaaaaabbbbbbbbbbccccccccccdddddddddd###############";
        Assert.assertEquals(96, this.tape3.tape.size());
        Assert.assertEquals(actualTape.chars().mapToObj(c -> (char) c).collect(Collectors.toList()), this.tape3.tape);
    }

    @Test
    public void moveTest()
    {
        try
        {
            Assert.assertEquals('a', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.LEFT);
            Assert.assertEquals('#', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('a', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('b', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('c', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('#', this.tape1.getCharUnderHead().charValue());
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    @Test
    public void moveOutOfBounds()
    {
        try
        {
            Assert.assertEquals('a', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.LEFT);
            Assert.assertEquals('#', this.tape1.getCharUnderHead().charValue());
            Assert.assertEquals(32, this.tape1.tape.size());
            this.tape1.move(Direction.LEFT);
            Assert.assertEquals(64, this.tape1.tape.size());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('#', this.tape1.getCharUnderHead().charValue());
            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals('a', this.tape1.getCharUnderHead().charValue());

            for(int i = 0; i < Tape._SIZE - 2; ++i)
                this.tape1.move(Direction.RIGHT);

            this.tape1.move(Direction.RIGHT);
            Assert.assertEquals(96, this.tape1.tape.size());
            Assert.assertEquals('#', this.tape1.getCharUnderHead().charValue());
        }
        catch(Exception e) {e.printStackTrace();}

    }

    @Test
    public void invalidOptionTest()
    {
        try
        {
            this.tape2.move(Direction.fromCharacter('x').orElse(null));
            Assert.fail();
        }
        catch(Exception e)
        {
            Assert.assertEquals("[ERROR] Invalid option! Available options: 'RIGHT' and 'LEFT'.", e.getMessage());
        }
    }
}
