package project.micslo.machine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import project.micslo.machine.config.MachineSeven;
import project.micslo.machine.state.Neighbour;
import project.micslo.machine.state.Node;

import java.io.File;
import java.io.IOException;

public class MachineRunnerTest {

    private MachineRunner machineRunner1;
    private MachineRunner machineRunner2;

    @Before
    public void init() throws IOException
    {
        MachineSeven seven1 = MachineSeven.load(new File("src/test/resources/unary_doubling.configuration"));
        machineRunner1 = new MachineRunner(MachineBuilder.build(seven1));

        MachineSeven seven2 = MachineSeven.load(new File("src/test/resources/example.configuration"));
        machineRunner2 = new MachineRunner(MachineBuilder.build(seven2));
    }

    @Test
    public void iterate_hasThreeNeighbours_returnsSecondNeighbour() throws Exception
    {
        Neighbour first = new Neighbour(new Node(1), 'q', 'q', 'P');
        Neighbour second = new Neighbour(new Node(2), '1', '1', 'L');
        Neighbour third = new Neighbour(new Node(3), 'w', 'w', 'P');
        Node node = new Node(0);
        node.addNeighbour(first);
        node.addNeighbour(second);
        node.addNeighbour(third);
        Assert.assertEquals(machineRunner1.iterate(node).get().getLabel(), second.getState().getLabel());
    }

    @Test
    public void runShouldReturnCorrectNumberOfIterations() throws Exception
    {
        Assert.assertEquals(4, machineRunner2.run(m -> {}, 10));
        Assert.assertEquals(52, machineRunner1.run(m -> {}, 100));
    }

    @Test
    public void runShouldIterate20timesInsteadOf52() throws Exception
    {
        Assert.assertEquals(20, machineRunner1.run(m -> {}, 20));
    }
}
