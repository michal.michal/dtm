package project.micslo.machine.state;

/**
 * Klasa reprezentujaca przejscie DO stanu.
 */
public class Neighbour
{
    private Node state;
    private Character read;
    private Character write;
    private Character move;

    public Neighbour(Node state, Character read, Character write, Character move)
    {
        this.state = state;
        this.read = read;
        this.write = write;
        this.move = move;
    }

    public Node getState() { return state; }

    public Character getRead() { return read; }

    public Character getWrite() { return write; }

    public Character getMove() { return move; }
}
