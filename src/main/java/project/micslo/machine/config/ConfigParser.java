package project.micslo.machine.config;

import project.micslo.utils.Relation;

import java.util.function.*;
import java.util.regex.Pattern;
import java.util.stream.*;

/**
 * Parser pliku, w ktorym znajduja sie parametry maszyny.
 */
public class ConfigParser implements Consumer<String>
{
    private Pattern _SPACE_PATTERN = Pattern.compile(" ");

    private String previousLine;
    private MachineSeven config;

    public ConfigParser(MachineSeven config)
    {
        this.config = config;
        this.previousLine = "";
    }

    /**
     * Ustawia kontekst parsowania.
     *
     * @param line Kontekst.
     */
    private void determineLine(String line)
    {
        if(line.equals("alfabet tasmowy:") || line.equals("alfabet wejsciowy:") || line.equals("slowo wejsciowe:") ||
                line.equals("stany:") || line.equals("stan poczatkowy:") || line.equals("stany akceptujace:") || line.equals("relacja przejscia:"))
            this.previousLine = line;
        else if(this.previousLine.equals("relacja przejscia:"))
            ;
        else
            this.previousLine = "";
    }

    /**
     * Parsuje parametry zleznie od kontekstu.
     *
     * @param line Parametry zalezne od kontekstu.
     */
    private void parse(String line)
    {
        if(this.previousLine.equals("alfabet tasmowy:"))
            this.config.alphabetTape = line.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        else if(this.previousLine.equals("alfabet wejsciowy:"))
            this.config.alphabetInput = line.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        else if(this.previousLine.equals("slowo wejsciowe:"))
            this.config.wordInput = line.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        else if(this.previousLine.equals("stany:"))
            this.config.states = _SPACE_PATTERN.splitAsStream(line).map(Integer::parseInt).collect(Collectors.toList());
        else if(this.previousLine.equals("stan poczatkowy:"))
            this.config.beginState = Integer.valueOf(line);
        else if(this.previousLine.equals("stany akceptujace:"))
            this.config.acceptStates = _SPACE_PATTERN.splitAsStream(line).map(Integer::parseInt).collect(Collectors.toList());
//            this.config.acceptStates = line.chars().mapToObj(Character::getNumericValue).collect(Collectors.toList());
        else if(this.previousLine.equals("relacja przejscia:"))
        {
            String[] params = line.split(" ");
            this.config.transitions.add(new Relation(Integer.valueOf(params[0]), params[1].charAt(0), Integer.valueOf(params[2]), params[3].charAt(0), params[4].charAt(0)));
        }

    }

    /**
     * Dokonuje wlasciwego parsowania pliku konfiguracyjnego z parametrami maszyny.
     *
     * @param line Pojedyncza linia z pliku.
     */
    @Override
    public void accept(String line)
    {
        parse(line);
        determineLine(line);
    }
}
