package project.micslo.UI;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.Caret;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import project.micslo.machine.Machine;
import project.micslo.machine.MachineBuilder;
import project.micslo.machine.MachineRunner;
import project.micslo.machine.config.MachineSeven;
import project.micslo.UI.logger.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Controller extends Application
{
    private final int _WINDOW_WIDTH = 800;
    private final int _WINDOW_HEIGHT = 600;

    private BorderPane layout;
    private HBox controls;
    private CodeArea text;
    private Button next;
    private CheckBox print;
    private MenuBar menuBar;
    private Menu file, help;
    private MenuItem loadMachine, saveMachineState, loadMachineState, legend;
    private Spinner<Integer> spinner;
    private VirtualizedScrollPane scrollText;
    private FileChooser fileChooser;
    private boolean running = false;

    private MachineRunner runner;
    private MachineSeven seven;
    private Machine machine;
    private Logger logger;


    public Controller()
    {
        this.seven = new MachineSeven();
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        primaryStage.setOnCloseRequest(e -> {Platform.exit(); System.exit(0);});
        primaryStage.setTitle("Deterministic Turing Machine");

        // Layouts
        this.layout = new BorderPane();
        this.layout.setStyle("-fx-background-color: #FFFFFF");
        this.controls = new HBox();
        this.controls.setAlignment(Pos.CENTER);
        this.controls.setPadding(new Insets(3));

        // Menus
        this.menuBar = new MenuBar();

        this.file = new Menu("File");
        this.loadMachine = new MenuItem("Load machine from text file");
        this.fileChooser = new FileChooser();
        this.fileChooser.setTitle("Load machine from text file");
        this.fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("All files", "*.txt"));
        this.loadMachine.setOnAction(e -> {
            File machineFilePath = this.fileChooser.showOpenDialog(primaryStage);
            try
            {
                // Ladowanie maszyny z pliku
                this.seven = MachineSeven.load(machineFilePath);

                // Budowanie maszyny
                this.machine = MachineBuilder.build(this.seven, this.logger);

                this.runner = new MachineRunner(this.machine, this.logger);

                // Wyczyszczenie terminala
                this.text.clear();
                this.text.appendText("Welcome to DTM (Deterministic Turing Machine)!");
                this.text.appendText("\n[INFO] File loaded successfully.");
            }
            catch(FileNotFoundException exception)
            {
                this.text.clear();
                this.text.appendText("Welcome to DTM (Deterministic Turing Machine)!");
                this.logger.nl().append("[ERROR] Error while loading a file.");
                showAlert("File not found", null, "File you tried to load does not exist.", Alert.AlertType.ERROR);
            }
            catch(IOException exception)
            {
                this.text.clear();
                this.text.appendText("Welcome to DTM (Deterministic Turing Machine)!");
                this.logger.nl().append("[ERROR] Error while loading a file.");
                showAlert("Error", null, "There were some problems trying to load file.", Alert.AlertType.ERROR);
            }
        });
        this.saveMachineState = new MenuItem("Save machine state");
        this.saveMachineState.setOnAction(e -> {
            this.logger.nl().append("[INFO] Machine state saved successfully.");
        });
        this.loadMachineState = new MenuItem("Load machine state");
        this.loadMachineState.setOnAction(e -> {
            this.logger.nl().append("[INFO] Machine state loaded successfully.");
        });
        this.file.getItems().addAll(loadMachine, saveMachineState, loadMachineState);

        this.help = new Menu("Help");
        this.legend = new MenuItem("Legend");
        this.legend.setOnAction(e -> {
            this.logger.nl().append("[INFO] Legend displayed.");
        });
        this.help.getItems().add(this.legend);
        this.menuBar.getMenus().addAll(file, help);
        this.layout.setTop(menuBar);

        // Control buttons
        this.next = new Button("Start");
        this.next.prefHeightProperty().bind(this.layout.heightProperty().divide(12));
        this.next.prefWidthProperty().bind(this.layout.widthProperty());
        this.next.getStyleClass().add("success");
        this.next.setOnMouseClicked(e -> {

            if(this.runner == null)
                this.logger.nl().append("[ERROR] Machine is not loaded.");
            else
            {
                switchButton();

                Runnable task = () -> {

                    if(this.print.isSelected())
                    {
                        this.runner.setPrint();
                        try
                        {
                            this.logger.nl().append("\nComputation length: " + Long.toUnsignedString(this.runner.run(m -> {
                                this.logger.nl().append(m.toString());
                                this.logger.nl().append("Head position: " + m.getTape().getHeadPos());
                            }, this.spinner.getValue())));
                        }
                        catch(Exception ex)
                        {
                            this.logger.append("[ERROR] There were some problems during execution of machine.");
                        }
                    }
                    else
                    {
                        this.runner.setDoNotPrint();
                        try
                        {
                            this.runner.run(m -> {}, this.spinner.getValue());
                        }
                        catch(Exception ex)
                        {
                            this.logger.nl().append("[ERROR] There were some problems during execution of machine.");
                        }
                    }

                    try
                    {
                        Thread.sleep(10);
                    }
                    catch(InterruptedException e1)
                    {
                        showAlert("Error", null, "There were some problems during program execution.", Alert.AlertType.ERROR);
                    }
                    Platform.runLater(this::switchButton);
                };

                Thread backgroundThread = new Thread(task);

                backgroundThread.setDaemon(false);
                backgroundThread.start();
            }
        });

        this.spinner = new Spinner<>(1, Integer.MAX_VALUE, 1);
        this.spinner.prefWidthProperty().bind(this.layout.widthProperty());
        this.spinner.prefHeightProperty().bind(this.layout.heightProperty().divide(12));
        this.spinner.setEditable(true);

        this.print = new CheckBox("Print ?");
        this.print.prefWidthProperty().bind(this.layout.widthProperty());
        this.print.prefHeightProperty().bind(this.layout.heightProperty().divide(12));
        this.print.alignmentProperty().setValue(Pos.CENTER);
        this.print.setSelected(true);

        // Visual grid
        //      Terminal
        this.text = new CodeArea();
        this.text.setEditable(false);
        this.text.appendText("Welcome to DTM (Deterministic Turing Machine) !");
        this.text.setParagraphGraphicFactory(LineNumberFactory.get(this.text));
        this.text.setWrapText(true);
        this.text.prefHeightProperty().bind(this.layout.heightProperty());
        this.text.prefWidthProperty().bind(this.layout.widthProperty());
        this.text.setShowCaret(Caret.CaretVisibility.OFF);
        this.scrollText = new VirtualizedScrollPane(this.text);

        // Logger
        this.logger = new Logger(this.text);

        // Layout the scene
        this.layout.setCenter(this.scrollText);
        this.controls.getChildren().addAll(this.next, this.spinner, this.print);
        this.layout.setBottom(this.controls);

        // Render
        Scene mainScene = new Scene(this.layout, _WINDOW_WIDTH, _WINDOW_HEIGHT);
        mainScene.getStylesheets().add(Controller.class.getResource("/bootstrap3.css").toExternalForm());
        primaryStage.setScene(mainScene);
        primaryStage.show();
        primaryStage.sizeToScene();
    }

    public void showAlert(String title, String header, String content, Alert.AlertType alertType)
    {
        Alert alert = new Alert(alertType, content, ButtonType.OK);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.showAndWait();
    }

    public void switchButton()
    {
        if(!this.running)
        {
            this.next.setText("Stop");
            this.next.getStyleClass().remove("success");
            this.next.getStyleClass().add("danger");
        }
        else
        {
            this.next.setText("Start");
            this.next.getStyleClass().remove("danger");
            this.next.getStyleClass().add("success");
        }
        this.running = !this.running;
    }

    public void run(String[] args) { launch(args); }
}
